<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function Form()
    {
        return view('Halaman.Form');
    }

    public function Submit(Request $request)
     {
        $Name = $request['Name'];
        $Address = $request['Address'];
        $Gender = $request['Gender'];
        $Nat = $request['Nat'];
        $Bio = $request['Bio'];

        return view('Halaman.Selamat', compact('Name', 'Address', 'Gender', 'Nat', 'Bio'));
     }  
}
