@extends('Layout.master')
@section('Table')
Halaman Registrasi
@endsection

@section('Isi')
<h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/Submit" method="post">
        @csrf
        <label>Name:</label> <br> <br>
        <input type="text" Name="Name"> <br> <br>
        <label>Address:</label> <br> <br>
        <textarea name="Address" id="" cols="30" rows="10"></textarea> <br> <br>
        <label for="">Gender</label> <br> <br>
        <input type="radio" name="Gender" >Male <br>
        <input type="radio" name="Gender" >Female <br>
        <input type="radio" name="Gender" >Other <br> <br>
        <label for="">Nationality:</label> <br> <br>
        <select name="Nat">
            <option value="1">Indonesian</option>
            <option value="2">Singaporean</option>
            <option value="3">Malaysian</option>
            <option value="4">Australian</option> <br> <br>
        </select> <br> <br>
        <label for="">Language Spoken:</label> <br> <br>
        <input type="checkbox"> Bahasa Indonesia <br>
        <input type="checkbox"> English <br>
        <input type="checkbox"> Other <br> <br>
        <label for="">Bio:</label> <br> <br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea> <br>

        
        <input type="Submit" value="Submit">
    </form>
@endsection